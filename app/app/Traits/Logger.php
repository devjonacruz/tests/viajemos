<?php

namespace App\Traits;

use Log;
use Exception;

trait Logger
{
    /**
     * Log Message
     *
     * @param Exception $e
     * @return void
     */
    public function log($e, $method, $class, $type = 'info', $params = [])
    {
        if (empty($params)) {
            $params = [
                $e->getMessage(),
                $e->getCode(),
                $method,
                $class
            ];
        }


        $message = sprintf("Error: %s Code:%s Method:%s Class:%s", $params[0], $params[1], $params[2], $params[3]);
        switch ($type) {
            case 'info':
                Log::info($message);
                break;
            case 'error':
                Log::error($message);
                break;
            case 'critical':
                Log::critical($message);
                break;
            default:
                Log::error($message);
        }
    }
}