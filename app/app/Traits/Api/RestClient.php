<?php

namespace App\Traits\Api;

use Exception;
use App\Models\Api;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use \GuzzleHttp\Exception\ServerException;
use \GuzzleHttp\Exception\ClientException;
use \GuzzleHttp\HandlerStack;
use \GuzzleHttp\Subscriber\Oauth\Oauth1;

trait RestClient
{
    private $apiData;

    private $endpoint;

    private $body;

    private $method;

    private $headers = [];

    public function request()
    {
        $response = [];
        try {
            $client = new Client();
            $options = [
                'timeout' => 0,
                'headers' => $this->getHeaders()
            ];

            if (strtoupper($this->getMethod()) != 'GET') {
                $options['body'] = json_encode($this->getBody());
            }

            $response = $client->request(
                $this->getMethod(),
                $this->getApiDataAttribute('url') . $this->getEndpoint(),
                $options
            );
        } catch (ClientException | ServerException $e) {
            $response = $e->getResponse();
        }
        return $response;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getApiData()
    {
        return $this->apiData;
    }

    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function setApiData($apiData)
    {
        $this->apiData = $apiData;
    }

    public function prepareApiRest($code, $enviroment = 'production')
    {
        $apiData = Api::where('name', $code)->where('type', $enviroment)->first();

        if (!$apiData) {
            throw new Exception("No estan configuradas las credenciales", "44");
        }

        $this->setApiData($apiData);
    }

    public function getApiDataAttribute($attribute)
    {
        return isset($this->getApiData()[$attribute]) ? $this->getApiData()[$attribute] : null;
    }

    public function setApiDataAttribute($attribute, $value)
    {
        $this->getApiData()->$attribute = $value;
    }

    public function prepareOauthAutorization($location)
    {

        $url = $this->getApiData()->url . $this->getEndpoint();

        $app_id = $this->getApiData()->client_app_id;
        $consumer_key = $this->getApiData()->client_id;
        $consumer_secret = $this->getApiData()->client_secret;

        $query = array(
            'location' => $location,
            'format' => 'json',
        );

        $oauth = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => uniqid(mt_rand(1, 1000)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );

        $base_info = $this->buildBaseString($url, $this->getMethod(), array_merge($query, $oauth));
        $composite_key = rawurlencode($consumer_secret) . '&';
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;

        $headers = $this->getHeaders();
        $headers['Authorization'] = $this->buildAuthorizationHeader($oauth);

        $this->setHeaders($headers);
    }

    public function buildBaseString($baseURI, $method, $params)
    {
        $r = array();
        ksort($params);
        foreach ($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    public function buildAuthorizationHeader($oauth)
    {
        $r = 'OAuth ';
        $values = array();
        foreach ($oauth as $key => $value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        $r .= implode(', ', $values);
        return $r;
    }
}
