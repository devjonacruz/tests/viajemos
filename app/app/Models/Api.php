<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    protected $table = 'apis';

    protected $fillable = [
        'name',
        'username',
        'password',
        'url',
        'type',
        'client_app_id',
        'client_id',
        'client_secret',
        'token',
        'refresh_token',
        'expiration_date',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'username',
        'password',
        'client_id',
        'client_secret',
        'refresh_token',
        'expiration_date',
        'created_at',
        'updated_at'
    ];

    protected $dates = [
        'expiration_date',
    ];
}
