<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class IsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('Authorization');

        if (!$header) {
            $request->attributes->set('user_id', 1);
            return $next($request);
        }
        
        $header = explode(' ', $header);
        $header = explode('|', $header[1]);
        
        $personal_access_token = DB::table('personal_access_tokens')->where('id', $header[0])->first();
        
        if (!$personal_access_token) {
            $request->attributes->set('user_id', 1);
            return $next($request);
        }
        
        $request->attributes->set('user_id', $personal_access_token->tokenable_id);

        return $next($request);
    }
}
