<?php

namespace App\Http\Controllers;

use App\Models\History;
use Illuminate\Http\Request;
use App\Traits\Api\RestClient;

class WeatherController extends Controller
{
    use RestClient;

    private $citysDefault = [
        'Miami' => [
            'lat' => 25.7616798,
            'lon' => -80.1917902,
        ],
        'Orlando' => [
            'lat' => 28.5383355,
            'lon' => -81.3792365,
        ],
        'NewYork' => [
            'lat' => 40.7127753,
            'lon' => -74.0059728,
        ],
    ];

    public function __construct()
    {
        $this->middleware([
            // 'auth:sanctum',
            'isUser',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->prepareApiRest('openweathermap', 'production');

        $res = [];

        foreach ($this->citysDefault as $city) {
            $city = (object)$city;
            $endpoint = "data/2.5/weather?units=metric&lat=$city->lat&lon=$city->lon&appid=" . $this->getApiDataAttribute('client_id');

            $this->setEndpoint($endpoint);

            $this->setMethod('GET');

            $this->setHeaders([
                'Content-Type' => 'application/json',
            ]);

            // $this->prepareOauthAutorization('sunnyvale,ca');

            $response = $this->request();

            $code = $response->getStatusCode();

            $res[] = json_decode($response->getBody(), true);

            History::create([
                'user_id' => $request->attributes->get('user_id'),
                'lat' => $city->lat,
                'lon' => $city->lon,
                'response' => $response->getBody(),
            ]);
        }

        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $lat, $lon)
    {
        $this->prepareApiRest('openweathermap', 'production');

        $endpoint = "data/2.5/weather?units=metric&lat=$lat&lon=$lon&appid=" . $this->getApiDataAttribute('client_id');

        $this->setEndpoint($endpoint);

        $this->setMethod('GET');

        $this->setHeaders([
            'Content-Type' => 'application/json',
        ]);

        // $this->prepareOauthAutorization('sunnyvale,ca');

        $response = $this->request();

        $code = $response->getStatusCode();

        $res = json_decode($response->getBody(), true);

        History::create([
            'user_id' => $request->attributes->get('user_id'),
            'lat' => $lat,
            'lon' => $lon,
            'response' => json_encode($res),
        ]);

        return response()->json($res);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
