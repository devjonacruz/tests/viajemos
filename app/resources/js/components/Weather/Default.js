import React from "react";
import ReactDOM from "react-dom";

class WeatherDefault extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            weather: []
        };
    }

    componentDidMount() {
        let token = document.querySelector('meta[name="api-token"]').content;
        let config = {
            method: "GET",
            headers: new Headers({
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            })
        };
        fetch("api/weather", config)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    weather: res
                });

                setTimeout(() => {
                    this._renderMap();
                }, 1000);
            });
    }

    _renderMapItem(data) {
        return (
            <div className="col-12 col-md-4 p-3" key={data.id}>
                <h2 className="text-center my-2">{data.name}</h2>
                <input
                    type="hidden"
                    name={`lat-${data.id}`}
                    id={`lat-${data.id}`}
                    value={data.coord.lat}
                />
                <input
                    type="hidden"
                    name={`lon-${data.id}`}
                    id={`lon-${data.id}`}
                    value={data.coord.lon}
                />
                <div id={`map-canvas-${data.id}`} style={styleSheet}></div>
                <div className="text-center py-3">
                    <p>
                        <strong>Temp:</strong>
                        {` ${data.main.temp}°`}
                    </p>
                    <p>
                        <strong>Temp Min:</strong>
                        {` ${data.main.temp_min}°`}
                    </p>
                    <p>
                        <strong>Temp Max:</strong>
                        {` ${data.main.temp_max}°`}
                    </p>
                    <p>
                        <strong>Humedad:</strong>
                        {` ${data.main.humidity}%`}
                    </p>
                </div>
            </div>
        );
    }

    _renderMap() {
        const { weather } = this.state;
        weather.map(data => {
            setMapGoogle(
                `#lat-${data.id}`,
                `#lon-${data.id}`,
                `#map-canvas-${data.id}`
            );
        });
    }

    render() {
        return this.state.weather.length > 0 ? (
            this.state.weather.map(w => this._renderMapItem(w))
        ) : (
            <div
                className="d-flex justify-content-center align-items-center"
                style={{ width: "100%", height: "500px" }}
            >
                Cargando
            </div>
        );
    }
}

const styleSheet = {
    width: "300px",
    height: "300px"
};

export default WeatherDefault;

if (document.getElementById("weather-default")) {
    ReactDOM.render(
        <WeatherDefault />,
        document.getElementById("weather-default")
    );
}
