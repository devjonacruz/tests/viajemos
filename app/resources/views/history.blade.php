@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Historial') }}</div>

                <div class="card-body">
                    <table class="table text-center table-responsive">
                        <thead>
                            <tr>
                                <th>Lugar</th>
                                <th>Latitud</th>
                                <th>Longitud</th>
                                <th>Respuesta</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($history as $item)
                            <tr>
                                <td><?= json_decode($item->response)->name ?></td>
                                <td><?= $item->lat ?></td>
                                <td><?= $item->lon ?></td>
                                <td>
                                    <p>
                                        <strong>Temp:</strong>
                                        <?=json_decode($item->response)->main->temp?>°
                                    </p>
                                    <p>
                                        <strong>Temp Min:</strong>
                                        <?=json_decode($item->response)->main->temp_min?>°
                                    </p>
                                    <p>
                                        <strong>Temp Max:</strong>
                                        <?=json_decode($item->response)->main->temp_max?>°
                                    </p>
                                    <p>
                                        <strong>Humedad:</strong>
                                        <?=json_decode($item->response)->main->humidity?>%
                                    </p>
                                    <pre>

                                    </pre>
                                </td>
                                <td><?= $item->created_at ?></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection