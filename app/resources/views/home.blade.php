@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row" id="weather-default">

            </div>
            <div class="row">
                <div class="col-12">
                    <form id="addressUser">
                        @csrf
                        <input type="hidden" name="latitude" id="latitude" value="00.00">
                        <input type="hidden" name="longitude" id="longitude" value="00.00">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Buscar lugar en el mundo" aria-label=""
                                aria-describedby="basic-addon2" id="location_input">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-10 mx-auto pb-5">
                    <div id="map-canvas" style="width: 600px; height: 400px;" class="mx-auto d-none">
                    </div>
                    <div id="map-canvas-sub" class="text-center py-3"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.onload = function() {
        initializeUbication('#location_input', '#latitude','#longitude','#map-canvas')
        let formAddressUser = document.getElementById('addressUser')
        formAddressUser.addEventListener('submit',function(e){
            e.preventDefault();

            let formData = new FormData(e.target);
            let token = document.querySelector('meta[name="api-token"]').content;
            let config = {
                method: 'GET',
                headers: new Headers({
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`,
                })
            }
            fetch(`api/weather/${formData.get('latitude')}/${formData.get('longitude')}`, config)
                .then(res => res.json())
                .then(res => {
                    let html = `
                    <div className="text-center py-3">
                        <p>
                            <strong>Temp:</strong>
                            ${res.main.temp}°
                        </p>
                        <p>
                            <strong>Temp Min:</strong>
                            ${res.main.temp_min}°
                        </p>
                        <p>
                            <strong>Temp Max:</strong>
                            ${res.main.temp_max}°
                        </p>
                        <p>
                            <strong>Humedad:</strong>
                            ${res.main.humidity}%
                        </p>
                    </div>
                `
                    let map = document.querySelector('#map-canvas-sub');
                    map.innerHTML = html
            });
        })
    }
</script>
@endsection