<?php

use Illuminate\Database\Seeder;
use App\Models\Api;

class ApiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Api::create([
            'name' => 'yahoo_weather',
            'type' => 'production',
            'url' => 'https://weather-ydn-yql.media.yahoo.com/',
            'client_app_id' => 'IhbJuH75',
            'client_id' => 'dj0yJmk9b29kcGFHT09ZMFNHJmQ9WVdrOVNXaGlTblZJTnpVbWNHbzlNQT09JnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWVl',
            'client_secret' => 'f3a760c801c46b7e812c24dce1a3ff3092ff4101s',
        ]);

        Api::create([
            'name' => 'openweathermap',
            'type' => 'production',
            'url' => 'https://api.openweathermap.org/',
            'client_id' => '5b8f463a6dcf3f97d10c3c1901ed6849',
        ]);
    }
}
